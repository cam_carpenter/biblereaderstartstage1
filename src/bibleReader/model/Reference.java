package bibleReader.model;

// TODO Add Javadoc comments throughout the class.

/**
 * A simple class that stores the book, chapter number, and verse number.
 * 
 * @author Charles Cusack (Skeleton Code), 2008, edited 2012.
 * @author ? (provided the implementation)
 */
public class Reference implements Comparable<Reference> {
	/*
	 * Add the appropriate fields and implement the methods. Notice that there
	 * are no setters. This is intentional since a reference shouldn't change.
	 * Important: You should NOT store the book as a string. There are several
	 * reasons for this that will be clear if you think about it for a few
	 * minutes. There is already a class that you can use instead.
	 */

	// TODO Add fields: Stage 2

	public Reference(BookOfBible book, int chapter, int verse) {
		// TODO Implement me: Stage 2
	}

	public String getBook() {
		// TODO Implement me: Stage 2
		return null;
	}

	public BookOfBible getBookOfBible() {
		// TODO Implement me: Stage 2
		return null;
	}

	public int getChapter() {
		// TODO Implement me: Stage 2
		return 0;
	}

	public int getVerse() {
		// TODO Implement me: Stage 2
		return 0;
	}

	/**
	 * This method should return the reference in the form: "book chapter:verse"
	 * For instance, "Genesis 2:3".
	 */
	@Override
	public String toString() {
		// TODO Implement me: Stage 2
		return null;
	}

	@Override
	public boolean equals(Object other) {
		// TODO Implement me: Stage 2
		// Don't forget the rules for how equals and hashCode are supposed to be
		// related.
		return false;
	}

	@Override
	public int hashCode() {
		// I'll give you this one as a freebie. This is a trick I learned from
		// Dr. McFall.
		// If two instances are the same, their toString method will return the
		// same thing (assuming you implement toString properly).
		// Thus calling hashCode on them will return the same int, so we are
		// honoring the contract that if a.equals(b) then
		// a.hashCode==b.hashCode.
		// Why go to extra trouble to implement hashCode based on the fields and
		// some weird formula when String already has an implementation?
		// (Well, maybe because it is perhaps less efficient, but we'll do it
		// anyway since it probably isn't that bad.)
		return toString().hashCode();
	}

	@Override
	public int compareTo(Reference otherRef) {
		// TODO Implement me: Stage 2
		// See the Javadoc for the Comparable interface for what this method is
		// supposed to do. Read the documentation for compareTo very carefully.
		//
		// Hint: Enums (like BookOfBible) implement the Comparable interface.
		// To compare int types, there are at least two good choices. One
		// involves using the Integer class (which implements the Comparable
		// interface). The other involves simple arithmetic and is simpler and
		// more efficient.
		return 0;
	}
}
